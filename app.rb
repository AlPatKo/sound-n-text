require 'gosu'

class GameWindow < Gosu::Window
  def initialize
    super 1200, 200, :update_interval => 90
    self.caption = "Savage Effects"
    #load my masterpiece
    @music = Gosu::Sample.new("masterpiece.wav")
    #play the shit out of it
    @music.play(1,1.25,true)

    #setup the message
    @message = ARGV[0].nil? ? "$W4G4L1C10U$" : ARGV[0]
    
    @font = Gosu::Font.new(40, :name => 'Courier')
    @x = (1200 - (@message.size+1)*27)/2
    @y = 75
    @scale_x = 2
    @scale_y = 3
    
    #create colors array for each letter
    @colors = []    
    step = 360/@message.size
    for i in 0..@message.size
      @colors << i*step
    end
    @c_id = 0
    @fwd = true
  end

  def update
    #swingin'
    if @fwd && @c_id < @message.size - 1
      @c_id = @c_id + 1
    elsif @fwd && @c_id == @message.size - 1
        @fwd = false
        @c_id = @c_id - 1
    elsif !@fwd && @c_id > 0
        @c_id = @c_id - 1
    elsif !@fwd && @c_id == 0
        @fwd = true
        @c_id = @c_id + 1
    end             
  end

  def draw
    # It ain't easy to be pimpin', jk coloring each char
    @message.chars.each_with_index do |c, i|
      if i < @c_id
        @font.draw(c, @x + i*30, @y, 1, 1, 1, 0xff_ffffff)
      elsif i == @c_id
        @font.draw(c, @x + i*30, @y - @scale_y*12, 1, @scale_x, @scale_y, Gosu::Color.from_ahsv(255,@colors[@c_id],1,1))
      else
        @font.draw(c, @x + i*30 + 10*@scale_x, @y,  1, 1, 1, 0xff_ffffff)
      end
    end
  end
end

window = GameWindow.new
window.show