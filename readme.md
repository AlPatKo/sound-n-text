# Laboratory work# MISSINGNO

It is the one where we were supposed to create a keygen thingy or, alternatively generate old school music and add some sick text effect.

I have picked the alternative, don't know why, maybe it is my artistic nature.

I used PySynth in order to create the sound track. It is fairly simple, and one might argue that it is not oldschool enough and that person will be terribly wrong, because what can be more oldschool than old nokia(yup before even symbian) sound synthesizer, so that is it. The melody is fairly simple, but it is not advised to listen way too much.

For text effects I used ruby and its library Gosu, which is used for gamedev. The script is, again fairly simple. I used Gosu's standard methods like update in order to make all the updates for text in real time. The effect is that the text is written in white on a black background, and then each letter is enhanced and changes color sequentially from start to finish and backwards. The colors are distributed evenly along the spectrum. Therefore, the longer the word, the smoother transition it will yield.

In order to run it one has only to run from terminal `$ ruby app.rb 'your text'`